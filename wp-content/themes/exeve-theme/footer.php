<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wpf
 */

?>

	</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="container">
    <div class="site-info">
      <p>&copy; Matrix Tooling Services Ltd <?php echo date('Y'); ?>, Ascot House, Lenton Street, Sandiacre, NG10 5DJ. All rights reserved.</p>
    </div><!-- .site-info -->    
  </div>
</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
