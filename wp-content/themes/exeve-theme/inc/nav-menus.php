<?php
/**
 * Nav Menus
 *
 * @package wpf
 */

/**
 * Main Menu
 */
function wpf_primary_menu() {
	$args = array(
		'theme_location'  => 'primary',
		'menu'            => 'Primary',
		'container'       => false,
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'list-bare',
		'menu_id'         => '',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => ''
	);
	wp_nav_menu( $args );
}