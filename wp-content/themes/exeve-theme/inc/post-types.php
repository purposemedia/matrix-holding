<?php
/**
 * Custom Post Types
 *
 * @package wpf
 */

/**
 * Shortcuts for sublime require: https://github.com/mboynes/super-cpt-bundle
 *
 * scpt: Add a filter for after_setup_theme and check to see if this plugin exists
 * cpt:  Add a new Super Custom Post Type
 * tax:  Add a new Super Custom Taxonomy
 * amb:  Add a new meta box for your Super Custom Post Type
 * ctat: Connect one or more Post Types with one or more Taxonomies
 * icon: Add an icon for your Custom Post Type
 * cpm:  Create a new Super_Custom_Post_Meta object around an existing post type (e.g. posts)
 */

function wpf_post_types() {
	if ( ! class_exists( 'Super_Custom_Post_Type' ) )
		return;
}

add_action( 'after_setup_theme', 'wpf_post_types' );



/**
 * Change posts per page for custom post types
 *
 *  Exmaple:
 *
 *  function wpf_custom_post_type_name_query($query)
 *  {
 *    if ($query->is_main_query() && $query->is_post_type_archive('custom-post-type-name') && !is_admin())
 *    $query->set('posts_per_page', -1);
 *  }
 *
 *  add_action('pre_get_posts', 'wpf_custom_post_type_name_query');
 *
 */