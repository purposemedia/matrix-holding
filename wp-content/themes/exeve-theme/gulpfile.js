/* ———————————————————————————————————————— */
/* LOAD GULP AND NPM REQUIRED MODULES
/* ———————————————————————————————————————— */

var gulp 								= require('gulp'),
		autoprefixer 				= require('autoprefixer'),
		del 								= require('del'),
		concat 							= require('gulp-concat'),
		cssNano 						= require('gulp-cssnano'),
		fileInclude 				= require('gulp-file-include'),
		imagemin 						= require('gulp-imagemin'),
		mqpacker          	= require('css-mqpacker'),
		modernizr 					= require('gulp-modernizr'),
		newer 							= require('gulp-newer'),
		plumber 						= require('gulp-plumber'),
		postcss 						= require('gulp-postcss'),
		rename 							= require('gulp-rename'),
		sourcemaps 					= require('gulp-sourcemaps'),
		uglify 							= require('gulp-uglify'),
		util 								= require('gulp-util'),
		pngquant 						= require('imagemin-pngquant'),
		lost 								= require('lost'),
		cssCalc 						= require('postcss-calc'),
		cssColorFunction 		= require('postcss-color-function'),
		cssConditionals 		= require('postcss-conditionals'),
		cssCustomMedia 			= require('postcss-custom-media'),
		cssDiscardComments 	= require('postcss-discard-comments'),
		cssImport 					= require('postcss-import'),
		cssMixins 					= require('postcss-mixins'),
		cssNested 					= require('postcss-nested'),
		cssSimpleExtend 		= require('postcss-simple-extend'),
		cssSimpleVars 			= require('postcss-simple-vars'),
    browserSync         = require('browser-sync').create(),
    reload              = browserSync.reload


/* ———————————————————————————————————————— */
/* CREATE AND STORE PATHS
/* ———————————————————————————————————————— */

var srcPath = {
	root: 'src/',
	bower: 'bower_components/',
	styles: 'src/css/',
	scripts: 'src/js/',
	images: 'src/images/'
};
var distPath = {
	root: './',
	styles: './',
	scripts: './js/',
	images: './images/'
};

/* ———————————————————————————————————————— */
/* CLEAN DIRECTORIES BEFORE RECOMPILE
/* ———————————————————————————————————————— */

// Clean dist css
gulp.task('styles:clean', function(cb){
	del([distPath.styles + '*.css'], cb);
});

// Clean dist css
gulp.task('scripts:clean', function(cb){
  del([distPath.scripts + '**/*.js'], cb);
});

/* ———————————————————————————————————————— */
/* PROCESS STYLESHEETS
/* ———————————————————————————————————————— */

gulp.task('styles:dev', function() {
	// Store PostCSS processors
	var processors = [
    cssImport(),
    cssCalc(),
    cssMixins(),
    cssSimpleVars({
      silent: true
    }),
    cssColorFunction(),
    cssNested(),
    cssCustomMedia(),
    lost(),
    mqpacker(),
    cssConditionals(),
    cssSimpleExtend(),
    cssDiscardComments(),
    autoprefixer({
      browsers: [
        'last 2 versions',
        'safari 5',
        'ie 9',
        'ie 10',
        'ie 11'
      ]
    })
	];
	// Return source css files
	return gulp.src(srcPath.styles + 'loader.css')
	// Return any errors
  .pipe(plumber({
    errorHandler: function (err) {
      console.log(err);
      this.emit('end');
    }
  }))
  // Initialize sourcemap
  .pipe(sourcemaps.init())
  // Process through postcss
  .pipe(postcss(processors))
  // Rename the file
  .pipe(rename('style.css'))
  // Write the sourcemap
  .pipe(sourcemaps.write('maps', {
    includeContent: false,
    sourceRoot: srcPath.styles + 'loader.css'
  }))
  // Write out the css file to dist
  .pipe(gulp.dest(distPath.styles))
});
// Main styles task which is triggered only when
// styles:dev has finished.
gulp.task('styles', ['styles:dev'], function() {
	// Return the styles.dev.css file in dist
  return gulp.src(distPath.styles + 'style.css')
  // Minify the file
  .pipe(cssNano())
  // Duplicate file and add .min onto filename
  .pipe(rename({suffix: '.min'}))
  // Write out file back to dist
  .pipe(gulp.dest(distPath.styles))
  .pipe(browserSync.stream());
});

/* ———————————————————————————————————————— */
/* PROCESS JAVASCRIPT
/* ———————————————————————————————————————— */

gulp.task('scripts', function() {
  // Return source js files
  return gulp.src([
    srcPath.scripts + 'patterns/accordion.js',
    srcPath.scripts + 'patterns/dropdown.js',
    srcPath.scripts + 'app.js'
  ])
	// Return any errors
  .pipe(plumber({
    errorHandler: function (err) {
      console.log(err);
      this.emit('end');
    }
  }))
  // Initialize sourcemap
  .pipe(sourcemaps.init())
  // Concatenate all the files into one
  .pipe(concat('app.js'))
  // write the sourcemap
  .pipe(sourcemaps.write({
    includeContent: false,
    sourceRoot: srcPath.scripts
  }))
  // Write out the js file to dist
  .pipe(gulp.dest(distPath.scripts))
  // Duplicate file and add .min onto filename
  .pipe(rename({suffix: '.min'}))
  // Minify the file
  .pipe(uglify())
  // Write out file back to dist
  .pipe(gulp.dest(distPath.scripts))
});

/* ———————————————————————————————————————— */
/* CREATE CUSTOM MODERNIZR
/* ———————————————————————————————————————— */

gulp.task('modernizr', ['scripts'], function() {
  // Return source js files
  return gulp.src(srcPath.scripts + '**/*.js')
  // Return any errors
  .pipe(plumber({
    errorHandler: function (err) {
      console.log(err);
      this.emit('end');
    }
  }))
  // Generate the custom modernizr file
  .pipe(modernizr('modernizr.min.js'))
  // Minify the file
  .pipe(uglify())
  // Write out file to dist
  .pipe(gulp.dest(distPath.scripts))
  .pipe(browserSync.stream());
});

/* ———————————————————————————————————————— */
/* PROCESS IMAGES
/* ———————————————————————————————————————— */

gulp.task('images', function() {
  // Return source images
  return gulp.src(srcPath.images + '**/*')
  // Only pass through newer images
  .pipe(newer(distPath.images))
  // Optimise the images
  .pipe(imagemin({
    progressive: true,
    svgoPlugins: [{removeViewBox: false}],
    use: [pngquant()]
  }))
  // Save the images to dist
  .pipe(gulp.dest(distPath.images))
  .pipe(browserSync.stream());
});

/* ———————————————————————————————————————— */
/* BROWSERSYNC
/* ———————————————————————————————————————— */

gulp.task('browser-sync', function() {
    browserSync.init(null, {
        proxy: "localhost/exeve/",
        notify: false
    });
});

/* ———————————————————————————————————————— */
/* RELOAD BROWSERS
/* ———————————————————————————————————————— */
gulp.task('reload', function() {
  browserSync.reload();
});

/* ———————————————————————————————————————— */
/* DEFAULT GULP TASK AND WATCHER
/* ———————————————————————————————————————— */

gulp.task('watch', function() {
  // Watch css for changes
  gulp.watch(srcPath.styles + '**/*.css', ['styles:clean', 'styles']).on('change', browserSync.reload);
  // Watch scripts for changes. Modernizr task runs
  // 'scripts' task first so no need to include it here
  gulp.watch(srcPath.scripts + '**/*.js', ['scripts:clean', 'modernizr']).on('change', browserSync.reload);
  // Watch images for changes
  gulp.watch(srcPath.images + '**/*', ['images']).on('change', browserSync.reload);
});

gulp.task('default', ['styles:clean', 'styles', 'scripts:clean', 'scripts', 'modernizr', 'images', 'browser-sync', 'watch']);