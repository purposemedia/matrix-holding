function accordion() {
  var allItems = $('.accordion__item');
  var allPanels = $('.accordion__content').hide();
  var firstPanel = $('.accordion__item:first-child');
  $(firstPanel).addClass('active').find('.accordion__content').show();
  $('.accordion__title').on(eventClickHandler, function() {
    $this = $(this);
    var target = $this.closest('.accordion__item');
    if (target.hasClass('active')) {
      target.removeClass('active').find('.accordion__content').slideUp();
    } else {
      allItems.removeClass('active');
      allPanels.slideUp();
      target.addClass('active');
      target.find('.accordion__content').slideDown();
    }
  });
}
function dropdown() {
  $('.dropdown-menu').hide();
  $('.dropdown-trigger').on(eventClickHandler, function() {
    $(this).toggleClass('active');
    $('.dropdown-menu').slideToggle();
    return false;
  });
}
/* ———————————————————————————————————————— */
/* Set jQuery no-conflict mode
/* ———————————————————————————————————————— */

var $ = jQuery.noConflict();

/* ———————————————————————————————————————— */
/* Definition of handler based on device  	*/
/* ———————————————————————————————————————— */

var eventClickHandler = 'click';
if ('ontouchstart' in document.documentElement) { clickHandler = 'eventClickHandler'; }

/* ———————————————————————————————————————— */
/* show more link  			                    */
/* ———————————————————————————————————————— */

function showmore() {
	// Click event example
  var icon = $('.showmore .fa');
	$('.showmore').on(eventClickHandler, function() {
    $(this).toggleClass('margin');
    $('.secondary-copy').slideToggle();
    icon.toggleClass( 'fa-caret-up', 'fa-caret-down' );
    return false;
	});
}

/* ———————————————————————————————————————— */
/* animations                               */
/* ———————————————————————————————————————— */

function animations() {
  $('.site-logo').addClass('animated fadeInDown');
  $('.tagline').addClass('animated fadeInLeft');
  $('.site-main .hentry').addClass('animated fadeInUp');
  $('.site-main .contact-form').addClass('animated fadeInUp');
  $('.site-footer').addClass('animated fadeInUp');
}


/* ———————————————————————————————————————— */
/* Global calls  														*/
/* ———————————————————————————————————————— */
// Shorthand document ready function
$(function() {
	accordion();
	dropdown();
  showmore();
  animations();
});




$("button").click(function(){
    $("p").hide("slow", function(){
        alert("The paragraph is now hidden");
    });
});









//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjY29yZGlvbi5qcyIsImRyb3Bkb3duLmpzIiwiYXBwLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZVJvb3QiOiJzcmMvanMvIn0=
