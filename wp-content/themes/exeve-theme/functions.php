<?php
/**
 * wpf functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wpf
 */

if ( ! function_exists( 'wpf_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wpf_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on wpf, use a find and replace
	 * to change 'wpf' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'wpf', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'wpf' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

}
endif;
add_action( 'after_setup_theme', 'wpf_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wpf_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wpf_content_width', 1170 );
}
add_action( 'after_setup_theme', 'wpf_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wpf_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'wpf' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wpf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wpf_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wpf_scripts() {
  $located = locate_template( 'style.min.css' );
	// Enqueue Stylesheet
  wp_enqueue_style( 'wpf-animate', get_stylesheet_directory_uri() . '/bower_components/animate.css/animate.min.css' );
  // If a style.min.css file exists, use that, otherwise use style.css
   if ($located != '' ) {
    echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/style.min.css" />';
   } else {
    wp_enqueue_style( 'wpf-style', get_stylesheet_uri() );
   }
	// Enqueue Scripts
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'wpf_modernizr', get_template_directory_uri() . '/js/modernizr.min.js', true );
	wp_enqueue_script( 'wpf_main', get_template_directory_uri() . '/js/app.min.js', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wpf_scripts' );


/**
 * Random cleanup items
 */

/* Remove the <p> tag from around images */
function wfp_filter_ptags_on_images($content) {
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

/* Remove the [...] to a read more link */
function wfp_excerpt_more($more) {
  global $post;
  // edit here if you like
	return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="'. __('Read', 'wfp') . get_the_title($post->ID).'">'. __('Read more &raquo;', 'wfp') .'</a>';
}

/* Remove 'text/css' from the enqueued stylesheets */
function wfp_style_remove($tag) {
  return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}
add_filter('style_loader_tag', 'wfp_style_remove');

/* Clean up wp_head() */
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'start_post_rel_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'adjacent_posts_rel_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );

/**
 * Spam comments
 * Automatically spam comments with a very long url
 */
function wfp_url_spamcheck( $approved , $commentdata ) {
  return ( strlen( $commentdata['comment_author_url'] ) > 50 ) ? 'spam' : $approved;
}
add_filter( 'pre_comment_approved', 'wfp_url_spamcheck', 99, 2 );

/* Change the footer in WordPress admin panel */
function wfp_remove_footer_admin () {
	printf( __( 'Theme Developed by %1$s.', 'wfp' ), 'Paul Godney' );
}
add_filter('admin_footer_text', 'wfp_remove_footer_admin');

/**
 * Hide theme update message.
 */
function wfp_hide_theme_updates( $r, $url ) {
    if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
        return $r; // Not a theme update request. Bail immediately.

    $themes = unserialize( $r['body']['themes'] );
    unset( $themes[ get_option( 'template' ) ] );
    unset( $themes[ get_option( 'stylesheet' ) ] );
    $r['body']['themes'] = serialize( $themes );
    return $r;
}

add_filter( 'http_request_args', 'wfp_hide_theme_updates', 5, 2 );

/**
 * Hide specific admin pages.
 */


/**
 * Hide Contact Form 7 menu item
 */
function remove_wpcf7() {
  remove_menu_page( 'wpcf7' );
}
add_action('admin_menu', 'remove_wpcf7');

/**
 * Hide custom fields menu item
 */
add_filter('acf/settings/show_admin', '__return_false');

/**
 * Hide specific admin subpages.
 */
function wfp_hide_theme_admin_subpages() {
  // $page[0] is the menu title
  // $page[1] is the minimum level or capability required
  // $page[2] is the URL to the item's file
  //$page = remove_submenu_page( 'themes.php', 'themes.php' );
}
add_action( 'admin_menu', 'wfp_hide_theme_admin_subpages', 999 );

/**
 * Remove Dashboard Widgets
 */
function wfp_remove_dashboard_meta() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
  remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal');
	remove_action('welcome_panel', 'wp_welcome_panel');
}
add_action( 'admin_init', 'wfp_remove_dashboard_meta' );

/**
 * Custom dashboard widgets
 */
function wfp_add_dashboard_widgets() {
	wp_add_dashboard_widget(
		'welcome_dashboard_widget', // Widget slug.
		'Hey '. get_bloginfo('name') .'. How are you today?',// Title.
		'wfp_dashboard_widget_function' // Display function.
	);
}
add_action( 'wp_dashboard_setup', 'wfp_add_dashboard_widgets' );

// Create the function to output the contents of our Dashboard Widget.
function wfp_dashboard_widget_function() {
  // Display whatever it is you want to show.
  echo '
  <h2 style="font-size: 18px; line-height: 1.3; border-bottom: 1px solid #eee; padding-bottom: 20px; margin-bottom: 0;">Welcome back to your holding website.</h2>
  <ul style="margin: 0; padding: 0;">
    <li style="border-bottom: 1px solid #eee; padding: 6px 0;"><a href="/wp-admin/post.php?post=4&action=edit" style="color: #0073aa; font-weight: bold;">› Edit the content on the holding page</a></li>
    <li style="border-bottom: 1px solid #eee; padding: 6px 0;"><a href="/wp-admin/admin.php?page=wpcf7&post=10&action=edit" style="color: #0073aa; font-weight: bold;">› Edit the holding page contact form</a></li>
    <li style="border-bottom: 1px solid #eee; padding: 6px 0;"><a href="/wp-admin/users.php" style="color: #0073aa; font-weight: bold;">› Manage your site administrators/users</a></li>
  </ul>
  ';
}

/**
 * Add ACF Options Pages
 */
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => 'Site Options',
    'menu_title'  => 'Site Options',
    'menu_slug'   => 'site-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  acf_add_options_sub_page(array(
    'page_title'  => 'Social Settings',
    'menu_title'  => 'Social Settings',
    'parent_slug' => 'site-settings',
  ));
}

/**
 * Hide ACF interface for client
 */
add_filter('acf/settings/show_admin', '__return_false');

/**
 * Custom theme template tags
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizr additions
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load predefined theme plugins
 */
require get_template_directory() . '/inc/plugins.php';

/**
 * Load theme navigation menus
 */
require get_template_directory() . '/inc/nav-menus.php';

/**
 * Load theme custom post types
 */
require get_template_directory() . '/inc/post-types.php';

/**
 * Redirect all pages to homepage while the site is just being used as a holding page.
 */
function wpse_76802_maintance_mode() {
  if ( ! is_page( 4 ) ) {
    wp_redirect( home_url( 'index.php?page_id=4' ) );
    exit;
  }
}
add_action( 'template_redirect', 'wpse_76802_maintance_mode' );

/**
 * Add bespoke CSS styling to the WordPress admin panel
 */
function my_custom_fonts() {
  echo '<style>
    .postbox { border: none; -webkit-box-shadow: none; box-shadow: none; background: rgba(255,255,255,0.7); padding: 20px; }
    .metabox-holder .postbox>h3, .metabox-holder .stuffbox>h3, .metabox-holder h2.hndle, .metabox-holder h3.hndle { font-size: 22px; }
  </style>';
}
add_action('admin_head', 'my_custom_fonts');
