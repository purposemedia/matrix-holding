<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wpf
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <!-- Specify UTF-8 character encoding
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">

  <!-- Display the webpage in edge mode, which is the
  highest standards mode supported by Internet Explorer,
  from Internet Explorer 6 through IE11.
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- Head meta tags
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Include Museo Sans Typekit Font
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!-- <script src="https://use.typekit.net/zvf4ymf.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script> -->

  <!-- Fontawesome CDN
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <script src="https://use.fontawesome.com/474ea7f37f.js"></script>

  <!-- XFN and pingback support
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <!-- wp_head() functions
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<?php wp_head(); ?></head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text visuallyhidden" href="#main"><?php esc_html_e( 'Skip to content', 'wpf' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
    <div class="container">
      <div class="site-branding">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-logo" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/images/Matrix-Tooling.png" alt="Go to homepage"></a>
      </div><!-- .site-branding -->
      <div class="tagline">
        <!-- <p>Specialists in Air Conditioning & Refrigeration</p> -->
      </div>
    </div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
