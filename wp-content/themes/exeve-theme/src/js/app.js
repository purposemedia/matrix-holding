/* ———————————————————————————————————————— */
/* Set jQuery no-conflict mode
/* ———————————————————————————————————————— */

var $ = jQuery.noConflict();

/* ———————————————————————————————————————— */
/* Definition of handler based on device  	*/
/* ———————————————————————————————————————— */

var eventClickHandler = 'click';
if ('ontouchstart' in document.documentElement) { clickHandler = 'eventClickHandler'; }

/* ———————————————————————————————————————— */
/* show more link  			                    */
/* ———————————————————————————————————————— */

function showmore() {
	// Click event example
  var icon = $('.showmore .fa');
	$('.showmore').on(eventClickHandler, function() {
    $(this).toggleClass('margin');
    $('.secondary-copy').slideToggle();
    icon.toggleClass( 'fa-caret-up', 'fa-caret-down' );
    return false;
	});
}

/* ———————————————————————————————————————— */
/* animations                               */
/* ———————————————————————————————————————— */

function animations() {
  $('.site-logo').addClass('animated fadeInDown');
  $('.tagline').addClass('animated fadeInLeft');
  $('.site-main .hentry').addClass('animated fadeInUp');
  $('.site-main .contact-form').addClass('animated fadeInUp');
  $('.site-footer').addClass('animated fadeInUp');
}


/* ———————————————————————————————————————— */
/* Global calls  														*/
/* ———————————————————————————————————————— */
// Shorthand document ready function
$(function() {
	accordion();
	dropdown();
  showmore();
  animations();
});




$("button").click(function(){
    $("p").hide("slow", function(){
        alert("The paragraph is now hidden");
    });
});








