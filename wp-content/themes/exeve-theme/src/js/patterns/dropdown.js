function dropdown() {
  $('.dropdown-menu').hide();
  $('.dropdown-trigger').on(eventClickHandler, function() {
    $(this).toggleClass('active');
    $('.dropdown-menu').slideToggle();
    return false;
  });
}