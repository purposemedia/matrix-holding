function accordion() {
  var allItems = $('.accordion__item');
  var allPanels = $('.accordion__content').hide();
  var firstPanel = $('.accordion__item:first-child');
  $(firstPanel).addClass('active').find('.accordion__content').show();
  $('.accordion__title').on(eventClickHandler, function() {
    $this = $(this);
    var target = $this.closest('.accordion__item');
    if (target.hasClass('active')) {
      target.removeClass('active').find('.accordion__content').slideUp();
    } else {
      allItems.removeClass('active');
      allPanels.slideUp();
      target.addClass('active');
      target.find('.accordion__content').slideDown();
    }
  });
}