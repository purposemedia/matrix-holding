<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wpf
 */

?>

<div class="container">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h2><?php the_field('main_title'); ?></h2>
		<p class="lead"><?php the_field('leading_text'); ?></p>
		<a href="#0" class="showmore">Read more <i class="fa fa-caret-down"></i></a>
		<div class="secondary-copy"><?php the_field('secondary_text'); ?></div>
	</article><!-- #post-## -->
	
	<div class="contact-form">
		<h2><?php the_field('get_in_touch_form_title'); ?></h2>
		<p><?php the_field('get_in_touch_form_text'); ?></p>
		<div class="form-wrapper">
			<?php echo do_shortcode('[contact-form-7 id="10" title="Contact form"]'); ?>
		</div>

	</div>

</div>
