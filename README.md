# README #

### How do I get set up? ###

* Clone the project to your local environment.
* Pull down the non-version controlled files via FTP
* Via terminal CD into the theme directory
* In the theme edit "config.json" to match the local domain you are using
* Run $ npm install
* Run $ bower install
* Run $ gulp

This will open a new browser window showing the holding page.

Login details for /wp-admin/ are contained within the customer database.